<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Account;
use App\Transacction;
use Log;
use DB;
use Illuminate\Support\Facades\Cache;
use PDO;
use Carbon\Carbon;

class ReportsController extends Controller
{
    public function __construct()
    {


    }
    public function list_account()
    {
        $user_id = Cache::get('user_id');
        $account = DB::select('select * from accounts where user_id = ? and available = ?  ORDER BY created_at DESC', [$user_id, 1]);
        return response()->json($account);

    }

    public function reports_between(Request $request)
    {
        $user_id = Cache::get('user_id');
        $account = Account::find($request->input('id_account'));

        $reports_income = Transacction::whereBetween('created_at', [$request->input('from'), $request->input('to')])
        ->where('user_id', $user_id)->where('id_account', $request->input('id_account'))->where('type', 'Expense')->sum('amount');

        $reports_expense = Transacction::whereBetween('created_at', [$request->input('from'), $request->input('to')])
        ->where('user_id', $user_id)->where('id_account', $request->input('id_account'))->where('type', 'Income')->sum('amount');

        return response()->json(['status'=>true, 'reports_income'=>$reports_income, 'reports_expense'=>$reports_expense, 'account'=>$account->small_name],200);

    }

    public function last_month()
    {

        $fecha = now();
        $month = $fecha->month;
        $year = $fecha->year;

        $user_id = Cache::get('user_id');
       // $account = Account::find($request->input('id_account'));

        /*$last_year_expense = Transacction::where('YEAR(created_at)', new Carbon(NOW()))
        ->where('MONTH(created_at)', MONTH(NOW()))->where('user_id', $user_id)->where('type', 'Expense')->sum('amount');*/

        $last_month_expense = Transacction::whereMonth('created_at', '=', $month)
        ->where('user_id', $user_id)->where('type', 'Expense')->sum('amount');

        $last_month_income = Transacction::whereMonth('created_at', '=', $month)
        ->where('user_id', $user_id)->where('type', 'Income')->sum('amount');

        $last_year_expense = Transacction::whereYear('created_at', '=', $year)
        ->where('user_id', $user_id)->where('type', 'Expense')->sum('amount');

        $last_year_income = Transacction::whereYear('created_at', '=', $year)
        ->where('user_id', $user_id)->where('type', 'Income')->sum('amount');


        return response()->json(['status'=>true,'reports_expense_month'=>$last_month_expense, 'reports_income_month'=>$last_month_income,
        'reports_expense_year'=>$last_year_expense, 'reports_income_year'=>$last_year_income],200);


    }


}
